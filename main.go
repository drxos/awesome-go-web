package main

import (
	"net/http"

	"gitlab.com/drxos/firstweb/utils"

	"gitlab.com/drxos/firstweb/models"

	"github.com/gorilla/mux"
)

func main() {
	models.Init()
	utils.LoadTemplates("templates/*.html")
	r := mux.NewRouter()
	http.Handle("/", r)
	http.ListenAndServe(":8080", nil)
}
